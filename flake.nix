{
  inputs = {
    naersk.url = "github:nmattia/naersk/master";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, naersk }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        naersk-lib = pkgs.callPackage naersk { };
        nativeBuildInputs = with pkgs; [ gtk4 pkg-config ];
        # TODO: This is obviously wrong... `libclang` is used for `stddef.h` because the one in `linuxHeaders` doesn't include `size_t`.
        # TODO: Look to https://discourse.nixos.org/t/setting-up-a-nix-env-that-can-compile-c-libraries/15833/3
        C_INCLUDE_PATH = with pkgs;
          "${glibc.dev}/include:${llvmPackages.libclang.lib}/lib/clang/${llvmPackages.libclang.version}/include";
        LIBCLANG_PATH = "${pkgs.llvmPackages.libclang.lib}/lib";
      in {
        defaultPackage = naersk-lib.buildPackage {
          root = ./.;

          inherit C_INCLUDE_PATH LIBCLANG_PATH nativeBuildInputs;
        };

        defaultApp = let
          drv = self.defaultPackage."${system}";
          name = pkgs.lib.strings.removeSuffix ("-" + drv.version) drv.name;
        in utils.lib.mkApp {
          inherit drv;
          # TODO: https://github.com/nix-community/naersk/issues/224
          exePath = "/bin/${name}";
        };

        devShell = with pkgs;
          mkShell {
            buildInputs =
              [ cargo rust-analyzer rustc rustfmt rustPackages.clippy ];

            RUST_SRC_PATH = rustPlatform.rustLibSrc;

            inherit C_INCLUDE_PATH LIBCLANG_PATH nativeBuildInputs;
          };
      });
}
