static LOG_INIT: std::sync::Once = std::sync::Once::new();

use i3_keyvisual::parser::*;

macro_rules! test_test_eq {
    ($name:ident, $commands:expr $(,)?) => {
        #[test]
        fn $name() {
            LOG_INIT.call_once(|| {
                flexi_logger::Logger::try_with_env()
                    .unwrap()
                    .start()
                    .unwrap();
            });
            assert_eq!(
                parse(concat!("tests/examples/tests/", stringify!($name), ".txt")).unwrap(),
                $commands,
            );
        }
    };
}

macro_rules! test_real_eq {
    ($name:ident) => {
        #[test]
        fn $name() {
            LOG_INIT.call_once(|| {
                flexi_logger::Logger::try_with_env()
                    .unwrap()
                    .start()
                    .unwrap();
            });
            let configuration = concat!(
                "tests/examples/real/checked/",
                stringify!($name),
                "/config.txt"
            );

            let ast_path = concat!(
                "tests/examples/real/checked/",
                stringify!($name),
                "/parsed.json"
            );
            let ast_file = std::fs::File::open(ast_path).unwrap();
            let ast_read = std::io::BufReader::new(ast_file);
            let ast: Vec<Configuration> = serde_json::from_reader(ast_read).unwrap();

            assert_eq!(parse(configuration).unwrap(), ast,);
        }
    };
}

macro_rules! test_real_succeeds {
    ($name:ident) => {
        #[test]
        fn $name() {
            LOG_INIT.call_once(|| {
                flexi_logger::Logger::try_with_env()
                    .unwrap()
                    .start()
                    .unwrap();
            });
            let configuration = concat!(
                "tests/examples/real/unchecked/",
                stringify!($name),
                "/config.txt"
            );

            parse(configuration).unwrap();
        }
    };
}

test_test_eq!(empty_comment, vec![Configuration::Comment("".into())],);

test_test_eq!(
    single_comment,
    vec![Configuration::Comment("This is a comment.".into())],
);

test_test_eq!(
    double_comment,
    vec![
        Configuration::Comment("First line.".into()),
        Configuration::Comment("Second line.".into())
    ],
);

test_test_eq!(
    comment_with_no_whitespace,
    vec![Configuration::Comment(
        "? A potential syntax for help comments.".into()
    )]
);

test_test_eq!(
    comment_with_continuation,
    vec![
        Configuration::Comment("First line. \\".into()),
        Configuration::Comment("Second line.".into())
    ],
);

test_test_eq!(
    simple_bindsym,
    vec![Configuration::Binding {
        key: "a".into(),
        command: "workspace b".into(),
        group: None,
        modifiers: vec![],
        options: vec![],
    }]
);

test_test_eq!(
    simple_bindcode,
    vec![Configuration::Binding {
        key: "214".into(),
        command: "exec --no-startup-id /home/michael/toggle_beamer.sh".into(),
        group: None,
        modifiers: vec![],
        options: vec![],
    }],
);

test_test_eq!(
    bindsym_with_modifier,
    vec![Configuration::Binding {
        key: "r".into(),
        command: "restart".into(),
        group: None,
        modifiers: vec![BindingModifier::Shift],
        options: vec![],
    }]
);

test_test_eq!(
    bindsym_with_two_modifiers,
    vec![Configuration::Binding {
        key: "r".into(),
        command: "restart".into(),
        group: None,
        modifiers: vec![BindingModifier::Mod(3), BindingModifier::Shift],
        options: vec![],
    }]
);

test_test_eq!(
    bindsym_with_group_and_modifiers,
    vec![Configuration::Binding {
        key: "XF86Tools".into(),
        command: r#"exec "notify-send \\"Hello, i3; from $USER\\"""#.into(),
        group: Some(BindingGroup(3)),
        modifiers: vec![BindingModifier::Mod(2), BindingModifier::Control],
        options: vec![],
    }]
);

test_test_eq!(
    mouse_binding,
    vec![Configuration::Binding {
        key: "Mouse1".into(),
        command: "floating toggle".into(),
        group: Some(BindingGroup(4)),
        modifiers: vec![BindingModifier::Mod(3), BindingModifier::Mod(1)],
        options: vec![BindingOption::Release, BindingOption::WholeWindow],
    }],
);

test_test_eq!(
    mode,
    vec![Configuration::Mode {
        name: "some_mode".into(),
        pango: true,
        configuration: vec![
            Configuration::Comment("A comment.".into()),
            Configuration::Binding {
                key: "a".into(),
                command: "restart".into(),
                group: None,
                modifiers: vec![],
                options: vec![],
            },
        ]
    }]
);

test_test_eq!(
    mode_quoted_and_escaped,
    vec![Configuration::Mode {
        name: r#"He said \\"Hello there!\\""#.into(),
        pango: false,
        configuration: vec![Configuration::Comment("Just a comment.".into())]
    }]
);

test_test_eq!(
    infinite_include_loop,
    vec![Configuration::Include(Include::Configuration(vec![]))],
);

test_test_eq!(
    include_one,
    vec![Configuration::Include(Include::Configuration(vec![
        Configuration::Binding {
            key: "a".into(),
            command: "workspace b".into(),
            group: None,
            modifiers: vec![],
            options: vec![],
        }
    ]))]
);

test_test_eq!(
    include_two_in_one,
    vec![Configuration::Include(Include::Configuration(vec![
        Configuration::Binding {
            key: "Mouse1".into(),
            command: "floating toggle".into(),
            group: Some(BindingGroup(4)),
            modifiers: vec![BindingModifier::Mod(3), BindingModifier::Mod(1)],
            options: vec![BindingOption::Release, BindingOption::WholeWindow],
        },
        Configuration::Binding {
            key: "214".into(),
            command: "exec --no-startup-id /home/michael/toggle_beamer.sh".into(),
            group: None,
            modifiers: vec![],
            options: vec![],
        }
    ]))]
);

test_test_eq!(
    include_two_in_two,
    vec![
        Configuration::Include(Include::Configuration(vec![Configuration::Binding {
            key: "Mouse1".into(),
            command: "floating toggle".into(),
            group: Some(BindingGroup(4)),
            modifiers: vec![BindingModifier::Mod(3), BindingModifier::Mod(1)],
            options: vec![BindingOption::Release, BindingOption::WholeWindow],
        }])),
        Configuration::Include(Include::Configuration(vec![Configuration::Binding {
            key: "214".into(),
            command: "exec --no-startup-id /home/michael/toggle_beamer.sh".into(),
            group: None,
            modifiers: vec![],
            options: vec![],
        }]))
    ]
);

test_test_eq!(
    include_without_quotes,
    vec![Configuration::Include(Include::Configuration(vec![
        Configuration::Binding {
            key: "a".into(),
            command: "workspace b".into(),
            group: None,
            modifiers: vec![],
            options: vec![],
        }
    ]))]
);

test_test_eq!(
    bar,
    vec![Configuration::Bar(vec![
        BarConfiguration::Other {
            directive: "status_command".into(),
            arguments: "i3status".into(),
        },
        BarConfiguration::Other {
            directive: "i3bar_command".into(),
            arguments: "i3bar".into(),
        },
        BarConfiguration::Colors(vec![
            ColorDirective {
                class: "background".into(),
                colors: vec!["FAFAFA".into()]
            },
            ColorDirective {
                class: "statusline".into(),
                colors: vec!["121417".into()]
            },
            ColorDirective {
                class: "separator".into(),
                colors: vec!["DBDBDC".into()]
            },
            ColorDirective {
                class: "focused_workspace".into(),
                colors: vec!["DBDBDC".into(), "FAFAFA".into(), "121417".into()]
            },
            ColorDirective {
                class: "active_workspace".into(),
                colors: vec!["DBDBDC".into(), "EAEAEB".into(), "121417".into()]
            },
            ColorDirective {
                class: "inactive_workspace".into(),
                colors: vec!["DBDBDC".into(), "EAEAEB".into(), "626772".into()]
            },
            ColorDirective {
                class: "urgent_workspace".into(),
                colors: vec!["CA1243".into(), "EAEAEB".into(), "626772".into()]
            },
            ColorDirective {
                class: "binding_mode".into(),
                colors: vec!["526FFF".into(), "FAFAFA".into(), "121417".into()]
            },
        ])
    ])]
);

test_real_eq!(sasasa);

test_real_eq!(seamsay);

test_real_eq!(simurgh);

test_real_succeeds!(arctorb);

test_real_succeeds!(beakers);

test_real_succeeds!(cmdywrtr27);

test_real_succeeds!(ice);

test_real_succeeds!(unixbhaskar);
