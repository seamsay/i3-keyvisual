font pango:FiraMono 8.000000
floating_modifier Mod4
default_border normal 2
default_floating_border normal 2
hide_edge_borders none
force_focus_wrapping no
focus_follows_mouse no
focus_on_window_activation smart
mouse_warping output
workspace_layout tabbed
workspace_auto_back_and_forth no

client.focused #DBDBDC #FAFAFA #121417 #9D9D9F #DBDBDC
client.focused_inactive #DBDBDC #EAEAEB #121417 #9D9D9F #DBDBDC
client.unfocused #DBDBDC #EAEAEB #626772 #9D9D9F #DBDBDC
client.urgent #CA1243 #EAEAEB #626772 #9D9D9F #CA1243
client.placeholder #DBDBDC #FAFAFA #121417 #9D9D9F #DBDBDC
client.background #FAFAFA


bindsym Mod4+0 workspace 10
bindsym Mod4+1 workspace 1
bindsym Mod4+2 workspace 2
bindsym Mod4+3 workspace 3
bindsym Mod4+4 workspace 4
bindsym Mod4+5 workspace 5
bindsym Mod4+6 workspace 6
bindsym Mod4+7 workspace 7
bindsym Mod4+8 workspace 8
bindsym Mod4+9 workspace 9
bindsym Mod4+Return exec rofi-sensible-terminal
bindsym Mod4+Shift+h workspace prev_on_output
bindsym Mod4+Shift+j focus child
bindsym Mod4+Shift+k focus parent
bindsym Mod4+Shift+l workspace next_on_output
bindsym Mod4+Tab focus mode_toggle
bindsym Mod4+a mode "Applications"
bindsym Mod4+b [title="^i3-vis:keybindings$"] scratchpad show, focus tiling
bindsym Mod4+c mode "Container"
bindsym Mod4+f fullscreen toggle
bindsym Mod4+h focus left
bindsym Mod4+j focus down
bindsym Mod4+k focus up
bindsym Mod4+l focus right
bindsym Mod4+m exec i3-treevisual --title Movement; focus tiling; mode "Move"
bindsym Mod4+q kill
bindsym Mod4+s mode "System"
bindsym Mod4+space exec "rofi -matching fuzzy -show drun -modi 'drun,run'"
bindsym Mod4+t exec i3-treevisual

mode "Applications" {
bindsym BackSpace mode "default"
bindsym Escape mode "default"
bindsym Return exec rofi-sensible-terminal, mode "default"
bindsym b exec firefox, mode "default"
bindsym e exec code, mode "default"
bindsym h split horizontal
bindsym m exec spotify, mode "default"
bindsym space exec "rofi -matching fuzzy -show drun -modi 'drun,run'", mode "default"
bindsym v split vertical
}

mode "Container" {
bindsym BackSpace mode "default"
bindsym Escape mode "default"
bindsym f floating toggle, mode "default"
bindsym h resize shrink width 10 px or 10 ppt
bindsym j resize shrink height 10 px or 10 ppt
bindsym k resize grow height 10 px or 10 ppt
bindsym l resize grow width 10 px or 10 ppt
bindsym p layout toggle split, mode "default"
bindsym s layout stacking, mode "default"
bindsym t layout tabbed, mode "default"
}

mode "Move" {
bindsym 0 move container to workspace 10, mode "default"
bindsym 1 move container to workspace 1, mode "default"
bindsym 2 move container to workspace 2, mode "default"
bindsym 3 move container to workspace 3, mode "default"
bindsym 4 move container to workspace 4, mode "default"
bindsym 5 move container to workspace 5, mode "default"
bindsym 6 move container to workspace 6, mode "default"
bindsym 7 move container to workspace 7, mode "default"
bindsym 8 move container to workspace 8, mode "default"
bindsym 9 move container to workspace 9, mode "default"
bindsym BackSpace [title="^i3-treevisual - Movement - "] kill, mode "default"
bindsym Escape [title="^i3-treevisual - Movement - "] kill, mode "default"
bindsym Shift+h focus prev sibling
bindsym Shift+j focus child
bindsym Shift+k focus parent
bindsym Shift+l focus next sibling
bindsym Shift+m focus prev sibling; focus child
bindsym Shift+n focus prev sibling; focus child
bindsym h exec i3-treeversal move left
bindsym j exec i3-treeversal move child
bindsym k exec i3-treeversal move parent
bindsym l exec i3-treeversal move right
bindsym m exec i3-treeversal move child-right
bindsym n exec i3-treeversal move child-left
}

mode "System" {
bindsym BackSpace mode "default"
bindsym Escape mode "default"
bindsym b exec --no-startup-id systemctl reboot
bindsym c reload
bindsym l exec --no-startup-id loginctl lock-session, mode "default"
bindsym q exit
bindsym r restart
bindsym s exec --no-startup-id systemctl poweroff
}


bar {

  font pango:monospace 8.000000



  status_command i3status
  i3bar_command i3bar



  colors {
    background #FAFAFA
    statusline #121417
    separator #DBDBDC



    focused_workspace #DBDBDC #FAFAFA #121417
    active_workspace #DBDBDC #EAEAEB #121417
    inactive_workspace #DBDBDC #EAEAEB #626772
    urgent_workspace #CA1243 #EAEAEB #626772
    binding_mode #526FFF #FAFAFA #121417
  }

}


for_window [title="^i3-vis:"] floating enable
for_window [class="^i3-treevisual$"] floating enable
for_window [title="^i3-vis:"] resize set width 70 ppt height 70 ppt
for_window [title="^i3-vis:"] move position center
for_window [title="^i3-vis:"] sticky enable
for_window [title="^i3-vis:"] move scratchpad
for_window [class="^i3-treevisual$"] resize set width 70 ppt height 70 ppt, move position center, sticky enable
for_window [all] title_window_icon yes
exec  i3-vis --no-tree

exec --no-startup-id numlockx on

exec --no-startup-id feh --no-fehbg --bg-scale nix-wallpaper-simple-light-gray.png

exec --no-startup-id i3-msg workspace 1



no_focus [title="^i3-treevisual - Movement - "]
