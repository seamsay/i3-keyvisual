mod implementation;

use color_eyre::eyre;
use gtk::gio;

glib::wrapper! {
    pub struct KeyVisual(ObjectSubclass<implementation::KeyVisual>)
        @extends gtk::Application,
        @implements gio::ActionGroup, gio::ActionMap, gio::Application, gtk::Buildable;
}

impl KeyVisual {
    pub fn new(id: &str) -> eyre::Result<Self> {
        log::debug!("Creating new application with id: {}", id);

        glib::Object::new(&[("application-id", &id)]).map_err(eyre::Report::from)
    }
}
