use color_eyre::eyre;
use glib::ParamSpec;
use gtk::gio::ApplicationFlags;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use i3_ipc::Connect;
use i3_keyvisual::parser;

use crate::notify::NotifyResult;

#[derive(Default)]
pub struct KeyVisual {
    // NOTE: `mode` is a property of the application because there can only be one mode regardless
    // of how many windows are open.
    pub mode: std::cell::RefCell<String>,
}

#[glib::object_subclass]
impl ObjectSubclass for KeyVisual {
    type ParentType = gtk::Application;
    type Type = super::KeyVisual;

    const NAME: &'static str = "I3KeyVisualKeyVisualKeyVisual";
}

impl ApplicationImpl for KeyVisual {
    fn activate(&self, application: &Self::Type) {
        self.parent_activate(application);

        let configuration = {
            let mut i3 =
                i3_ipc::I3::connect().expect("Can't recover from being unable to connect.");
            i3.get_config()
        };

        let configuration = match configuration {
            Ok(ref configuration) => match configuration.included_configs {
                Some(ref included) => included
                    .iter()
                    .flat_map(|included| {
                        parser::parse(&included.variable_replaced_contents[..])
                            .expect("Can't recover from being unable to parse configuration.")
                    })
                    .collect::<Vec<_>>(),
                None => parser::parse(&configuration.config[..])
                    .expect("Can't recover from being unable to parse configuration."),
            },
            // TODO: Can we check to see if this is due to i3 being too old or another error?
            Err(_) => todo!("Search the standard paths."),
        };

        let window =
            crate::configuration_window::ConfigurationWindow::new(application, configuration);
        let notebook = window
            .child()
            .expect("`ConfigurationWindow` must have a child.")
            .downcast::<gtk::Notebook>()
            .expect("Child must be a `gtk::Notebook`.");

        application
            .bind_property("mode", &notebook, "page")
            .transform_to(|binding, value| {
                let notebook = binding
                    .target()
                    .expect("Unable to recover from the notebook being deallocated.")
                    .downcast::<gtk::Notebook>()
                    .expect("The target should be a notebook.");

                let mode = value
                    .get::<String>()
                    .expect("The `mode` property should be convertible to a `String`.");

                let pages = notebook.pages();

                let mut index = None;
                for i in 0..notebook.n_pages() {
                    let page = pages
                        .item(i)
                        .expect("`n_pages` should only return in-bounds indices.")
                        .downcast::<gtk::NotebookPage>()
                        .expect("The `ListModel` should contain `NotebookPages`.")
                        .child();

                    let text = notebook
                        .tab_label_text(&page)
                        .expect("Each tab should have a label.");
                    let text = text.as_str();

                    if text == mode {
                        index = Some(
                            i32::try_from(i)
                                .expect(
                                    "If there are this many tabs we probably have bigger \
                                     problems...",
                                )
                                .to_value(),
                        );
                        break;
                    }
                }

                index
            })
            .flags(glib::BindingFlags::SYNC_CREATE)
            .build();

        window.show();
    }

    fn handle_local_options(&self, application: &Self::Type, options: &glib::VariantDict) -> i32 {
        const CONTINUE: i32 = -1;

        self.parent_handle_local_options(application, options);

        let level = options
            .lookup::<String>("log-level")
            .expect("GTK should guarantee this is a string.")
            .unwrap_or_else(|| "info".into());

        let directory = options
            .lookup::<std::path::PathBuf>("log-directory")
            .expect("GTK should guarantee this is the correct type.")
            .unwrap_or_else(|| {
                directories::ProjectDirs::from(
                    crate::QUALIFIER,
                    crate::ORGANISATION,
                    crate::APPLICATION,
                )
                .expect("A setup where this returns `None` would not be able to run i3.")
                .state_dir()
                .expect("This directory always exists on Linux.")
                .into()
            });

        flexi_logger::Logger::try_with_env_or_str(level)
            .unwrap_notify(application)
                .adaptive_format_for_stderr(flexi_logger::AdaptiveFormat::Custom(
                    crate::log_format,
                    crate::coloured_log_format,
                ))
                .format_for_files(crate::log_format)
                .log_to_file(flexi_logger::FileSpec::default().directory(&directory))
                .duplicate_to_stderr(flexi_logger::Duplicate::All)
                .start()
                .unwrap_notify(application);

        glib::log_set_default_handler(glib::rust_log_handler);

        log::info!("Log directory: {}", directory.display());

        CONTINUE
    }

    fn open(&self, application: &Self::Type, files: &[gtk::gio::File], hint: &str) {
        assert_eq!(hint, "");

        // TODO: Could we instead implement `Parseable` for `File`?
        for file in files {
            if let Some(window) = file
                .load_contents(None::<&gtk::gio::Cancellable>)
                .map_err(eyre::Report::from)
                .and_then(|(contents, _etag)| {
                    String::from_utf8(contents).map_err(eyre::Report::from)
                })
                .and_then(|contents| parser::parse(contents.as_str()).map_err(eyre::Report::from))
                .map(|configuration| {
                    crate::configuration_window::ConfigurationWindow::new(
                        application,
                        configuration,
                    )
                })
                .or_notify(application)
            {
                window.show();
            }
        }
    }

    fn startup(&self, application: &Self::Type) {
        self.parent_startup(application);

        let (transmitter, receiver) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);

        {
            let mut i3 =
                i3_ipc::I3::connect().expect("Can't recover from being unable to connect.");
            let mode = i3.get_binding_state().unwrap().name;
            transmitter
                .send(mode)
                .expect("Can't recover from being unable to send the mode.");
        }

        std::thread::spawn(move || {
            let mut i3 = i3_ipc::I3Stream::conn_sub(&[i3_ipc::event::Subscribe::Mode])
                .expect("Can't recover from being unable to connect.");

            for event in i3.listen() {
                let event = event.unwrap();
                match event {
                    i3_ipc::event::Event::Mode(data) => {
                        log::debug!("Received a mode change event: {:?}", data);
                        transmitter
                            .send(data.change)
                            .expect("Can't recover from being unable to send the mode.");
                    }
                    other => unreachable!("Shouldn't be subscribed to event: {:?}", other),
                }
            }
        });

        receiver.attach(
            None,
            glib::clone!(@weak application => @default-panic, move |message| {
                application.set_property("mode", message);
                glib::Continue(true)
            }),
        );
    }
}

impl GtkApplicationImpl for KeyVisual {}

impl ObjectImpl for KeyVisual {
    fn properties() -> &'static [glib::ParamSpec] {
        static PROPERTIES: once_cell::sync::Lazy<Vec<glib::ParamSpec>> =
            once_cell::sync::Lazy::new(|| {
                vec![glib::ParamSpecString::builder("mode")
                    .blurb("The current i3 binding mode.")
                    .default_value(Some("default"))
                    .flags(glib::ParamFlags::READWRITE | glib::ParamFlags::CONSTRUCT)
                    .build()]
            });

        PROPERTIES.as_ref()
    }

    fn set_property(&self, _obj: &Self::Type, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
        match pspec.name() {
            "mode" => {
                let mode = value
                    .get()
                    .expect("GTK should handle making sure this works.");
                *self.mode.borrow_mut() = mode;
            }
            _ => unimplemented!(),
        }
    }

    fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> glib::Value {
        match pspec.name() {
            "mode" => self.mode.borrow().to_value(),
            _ => unimplemented!(),
        }
    }

    fn constructed(&self, obj: &Self::Type) {
        self.parent_constructed(obj);

        *self.mode.borrow_mut() = "default".into();

        obj.add_main_option(
            "log-level",
            'l'.try_into().expect("Hardcoded character is valid ASCII."),
            glib::OptionFlags::NONE,
            glib::OptionArg::String,
            "Set the minimum level of logs to print.",
            Some("[trace, debug, info, warn, error]"),
        );

        obj.add_main_option(
            "log-directory",
            'd'.try_into().expect("Hardcoded character is valid ASCII."),
            glib::OptionFlags::NONE,
            glib::OptionArg::Filename,
            "Directory in which to create log files.",
            Some("<a writeable directory>"),
        );

        obj.set_flags(ApplicationFlags::HANDLES_OPEN);
    }
}
