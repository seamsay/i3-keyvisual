use color_eyre::eyre;
use gtk::glib;
use gtk::prelude::*;

mod binding_object;
mod configuration_window;
mod key_visual;
mod notify;

fn coloured_log_format(
    w: &mut dyn std::io::Write,
    now: &mut flexi_logger::DeferredNow,
    record: &flexi_logger::Record,
) -> Result<(), std::io::Error> {
    let level = record.level();
    // TODO: Make the level fixed width (`:5` doesn't work).
    write!(
        w,
        "[{}] {} [{}] {}",
        flexi_logger::style(level)
            .paint(now.format(flexi_logger::TS_DASHES_BLANK_COLONS_DOT_BLANK)),
        flexi_logger::style(level).paint(record.level().to_string()),
        record.module_path().unwrap_or("<unnamed>"),
        flexi_logger::style(level).paint(&record.args().to_string()),
    )
}

fn log_format(
    w: &mut dyn std::io::Write,
    now: &mut flexi_logger::DeferredNow,
    record: &flexi_logger::Record,
) -> Result<(), std::io::Error> {
    write!(
        w,
        "[{}] {} [{}] {}",
        now.format(flexi_logger::TS_DASHES_BLANK_COLONS_DOT_BLANK),
        record.level(),
        record.module_path().unwrap_or("<unnamed>"),
        &record.args(),
    )
}

const QUALIFIER: &str = "xyz";
const ORGANISATION: &str = "seamsay";
const APPLICATION: &str = env!("CARGO_PKG_NAME");

fn main() -> eyre::Result<()> {
    color_eyre::install()?;

    // This is needed so that we can match on class in i3.
    glib::set_program_name(Some("i3-keyvisual"));

    let app =
        key_visual::KeyVisual::new(&format!("{}.{}.{}", QUALIFIER, ORGANISATION, APPLICATION))?;

    std::process::exit(app.run());
}
