use gtk::subclass::prelude::*;

#[derive(Default)]
pub struct ConfigurationWindow {}

#[glib::object_subclass]
impl ObjectSubclass for ConfigurationWindow {
    type ParentType = gtk::ApplicationWindow;
    type Type = super::ConfigurationWindow;

    const NAME: &'static str = "I3KeyVisualConfigurationWindowConfigurationWindow";
}

impl ApplicationWindowImpl for ConfigurationWindow {}
impl ObjectImpl for ConfigurationWindow {}
impl WidgetImpl for ConfigurationWindow {}
impl WindowImpl for ConfigurationWindow {}
