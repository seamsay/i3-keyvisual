use std::collections::HashMap;

use gtk::prelude::*;
use gtk::subclass::prelude::*;
use i3_keyvisual::parser;

mod implementation;

macro_rules! column {
    {$view:expr, $header:expr, $item:ident, $binding:ident, $($implementation:tt)+} => {{
        let factory = gtk::SignalListItemFactory::new();

        factory.connect_setup(|_, item| {
            let label = gtk::Label::builder().halign(gtk::Align::Start).build();
            item.set_child(Some(&label));
        });

        factory.connect_bind(|_, $item| {
            let binding = $item
                .item()
                .unwrap()
                .downcast::<crate::binding_object::BindingObject>()
                .unwrap();
            let $binding = binding.imp();

            $($implementation)+
        });

        let column = gtk::ColumnViewColumn::new(Some($header), Some(&factory));
        $view.append_column(&column);
    }}
}

glib::wrapper! {
    pub struct ConfigurationWindow(ObjectSubclass<implementation::ConfigurationWindow>)
        @extends gtk::ApplicationWindow, gtk::Window, gtk::Widget,
        @implements gtk::gio::ActionGroup, gtk::gio::ActionMap, gtk::Accessible, gtk::Buildable,
            gtk::ConstraintTarget, gtk::Native, gtk::Root, gtk::ShortcutManager;
}

impl ConfigurationWindow {
    pub fn new(
        application: &crate::key_visual::KeyVisual,
        configuration: Vec<i3_keyvisual::parser::Configuration>,
    ) -> Self {
        let flattened = {
            let mut configuration = configuration;
            let mut flattened = vec![];
            while let Some(item) = configuration.pop() {
                match item {
                    parser::Configuration::Include(include) => match include {
                        parser::Include::Configuration(included) => {
                            configuration.extend(included.into_iter())
                        }
                        parser::Include::Pattern(pattern) => log::warn!(
                            "Unable to read configuration files included by: {}",
                            pattern
                        ),
                    },
                    other => flattened.push(other),
                }
            }
            flattened
        };

        let bindings = {
            let mut stack = vec![("default".to_owned(), flattened)];
            let mut bindings: HashMap<_, Vec<_>> = HashMap::new();

            while let Some((mode, configurations)) = stack.pop() {
                for item in configurations {
                    match item {
                        parser::Configuration::Binding {
                            command,
                            key,
                            modifiers,
                            group,
                            options,
                        } => {
                            let binding = crate::binding_object::BindingObject::new(
                                group, modifiers, key, options, command,
                            );
                            if let Some(list) = bindings.get_mut(&mode) {
                                list.push(binding);
                            } else {
                                bindings.insert(mode.clone(), vec![binding]);
                            }
                        }
                        parser::Configuration::Mode {
                            name,
                            configuration,
                            ..
                        } => stack.push((name, configuration)),
                        parser::Configuration::Include(..) => {
                            unreachable!("Includes should have been removed on flattening.")
                        }
                        _ => {}
                    }
                }
            }

            let mut bindings = bindings.into_iter().collect::<Vec<_>>();
            bindings.sort_unstable_by_key(|(mode, _)| {
                (if mode == "default" { 0 } else { 1 }, mode.clone())
            });

            bindings
        };

        let notebook = gtk::Notebook::builder()
            .enable_popup(true)
            .scrollable(true)
            .build();

        for (mode, bindings) in bindings {
            let model =
                gtk::gio::ListStore::new(crate::binding_object::BindingObject::static_type());
            model.splice(0, 0, &bindings);

            let model = gtk::NoSelection::new(Some(&model));

            let view = gtk::ColumnView::builder().model(&model).build();

            column! {
                view,
                "Group",
                item,
                binding,
                if let Some(ref group) = *binding.group.borrow() {
                    let label = item.child().unwrap().downcast::<gtk::Label>().unwrap();
                    label.set_text(&format!("{}", group));
                };
            };
            column! {
                view,
                "Modifiers",
                item,
                binding,
                let modifiers = binding.modifiers.borrow();

                if !modifiers.is_empty() {
                    let label = item.child().unwrap().downcast::<gtk::Label>().unwrap();
                    let modifiers = modifiers
                        .iter()
                        .map(|modifier| format!("{}", modifier))
                        .collect::<Vec<_>>()
                        .join("+");
                    label.set_text(&modifiers);
                };
            }
            column! {
                view,
                "Key",
                item,
                binding,
                let label = item.child().unwrap().downcast::<gtk::Label>().unwrap();

                label.set_text(binding.key.borrow().as_str());
            }
            column! {
                view,
                "Options",
                item,
                binding,
                let options = binding.options.borrow();

                if !options.is_empty() {
                    let label = item.child().unwrap().downcast::<gtk::Label>().unwrap();
                    let options = options
                        .iter()
                        .map(|option| format!("{}", option))
                        .collect::<Vec<_>>()
                        .join(", ");
                    label.set_text(&options);
                };
            }
            column! {
                view,
                "Command",
                item,
                binding,
                let label = item.child().unwrap().downcast::<gtk::Label>().unwrap();

                label.set_text(binding.command.borrow().as_str());
            }

            let scroller = gtk::ScrolledWindow::builder().child(&view).build();

            notebook.append_page(&scroller, Some(&gtk::Label::new(Some(mode.as_str()))));
        }

        let window: ConfigurationWindow = glib::Object::new(&[
            ("application", application),
            ("child", &notebook),
            ("default-height", &200 as &dyn ToValue),
            ("default-width", &300 as &dyn ToValue),
            ("title", &crate::APPLICATION),
        ])
        .unwrap();

        window
    }
}
