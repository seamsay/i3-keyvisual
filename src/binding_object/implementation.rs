use std::cell::RefCell;

use gtk::subclass::prelude::*;
use i3_keyvisual::parser;

#[derive(Default)]
pub struct BindingObject {
    pub group: RefCell<Option<parser::BindingGroup>>,
    pub modifiers: RefCell<Vec<parser::BindingModifier>>,
    pub key: RefCell<String>,
    pub options: RefCell<Vec<parser::BindingOption>>,
    pub command: RefCell<String>,
}

#[glib::object_subclass]
impl ObjectSubclass for BindingObject {
    type Type = super::BindingObject;

    const NAME: &'static str = "I3KeyVisualBindingObjectBindingObject";
}

impl ObjectImpl for BindingObject {}
