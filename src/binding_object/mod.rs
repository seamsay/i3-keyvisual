use gtk::subclass::prelude::*;
use i3_keyvisual::parser;

mod implementation;

glib::wrapper! {
    pub struct BindingObject(ObjectSubclass<implementation::BindingObject>);
}

impl BindingObject {
    pub fn new(
        group: Option<parser::BindingGroup>,
        modifiers: Vec<parser::BindingModifier>,
        key: String,
        options: Vec<parser::BindingOption>,
        command: String,
    ) -> Self {
        let object: BindingObject = glib::Object::new(&[]).unwrap();

        // NOTE: Using `imp` because this object should be considered immutable (i.e.
        // doesn't implement `set_property`).
        *object.imp().group.borrow_mut() = group;
        *object.imp().modifiers.borrow_mut() = modifiers;
        *object.imp().key.borrow_mut() = key;
        *object.imp().options.borrow_mut() = options;
        *object.imp().command.borrow_mut() = command;

        object
    }
}
