use std::hash::{Hash, Hasher};

use gtk::gio;
use gtk::prelude::ApplicationExt;

fn hash<T: Hash>(value: &T) -> u64 {
    let mut hasher = std::collections::hash_map::DefaultHasher::new();
    value.hash(&mut hasher);
    hasher.finish()
}

pub trait NotifyResult<T> {
    fn or_notify(self, app: &crate::key_visual::KeyVisual) -> Option<T>;

    fn unwrap_notify(self, app: &crate::key_visual::KeyVisual) -> T where Self: std::marker::Sized {
        self.or_notify(app).unwrap()
    }
}

impl<T, E: std::fmt::Debug + std::fmt::Display> NotifyResult<T> for Result<T, E> {
    fn or_notify(self, app: &crate::key_visual::KeyVisual) -> Option<T> {
        let error = match self {
            Ok(value) => return Some(value),
            Err(error) => error,
        };

        let notification_text = format!("{}", error);
        let log_text = format!("{:?}", error);

        let notification = gio::Notification::new("Error! See log file...");
        notification.set_body(Some(&notification_text));
        notification.set_priority(gio::NotificationPriority::Urgent);

        for line in log_text.split('\n') {
            log::error!("{}", line);
        }

        app.send_notification(
            Some(&format!("error-notification-{}", hash(&notification))),
            &notification,
        );

        None
    }
}
