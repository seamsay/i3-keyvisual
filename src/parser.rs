use std::collections::HashMap;
use std::fmt::Display;
use std::io::Read;
use std::str::{CharIndices, Chars};

use nom::branch::alt;
use nom::bytes::complete::{is_not, tag, tag_no_case};
use nom::character::complete::{alphanumeric1, line_ending, multispace0, not_line_ending, space0};
use nom::combinator::{map, opt};
use nom::multi::{many0, many1};
use nom::sequence::{pair, tuple};
use nom::{
    AsBytes, AsChar, Compare, InputIter, InputLength, InputTake, InputTakeAtPosition, Slice,
    UnspecializedInput,
};
use regex::Regex;
use serde::{Deserialize, Serialize};

static CHDIR_MUTEX: once_cell::sync::Lazy<std::sync::Mutex<()>> =
    once_cell::sync::Lazy::new(|| std::sync::Mutex::new(()));

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
struct VariableName(String);
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
struct VariableValue(String);

// NOTE: Most of these implementations are taken almost wholesale from nom's
// internal implementations for `&str`.
#[derive(Clone, Debug)]
struct ParseState<'c, 'd, 'i, 'v> {
    contents: &'c str,
    directory: Option<&'d std::path::Path>,
    // TODO: Surely `RefCell` isn't needed...
    included: &'i std::cell::RefCell<std::collections::HashSet<std::path::PathBuf>>,
    variables: &'v HashMap<VariableName, VariableValue>,
}

impl<'c, 'd, 'i, 'v> AsBytes for ParseState<'c, 'd, 'i, 'v> {
    fn as_bytes(&self) -> &[u8] {
        self.contents.as_bytes()
    }
}

impl<'t, 'c, 'd, 'i, 'v, T> Compare<T> for ParseState<'c, 'd, 'i, 'v>
where
    &'c str: Compare<T>,
{
    fn compare(&self, t: T) -> nom::CompareResult {
        self.contents.compare(t)
    }

    fn compare_no_case(&self, t: T) -> nom::CompareResult {
        self.contents.compare_no_case(t)
    }
}

impl<'c, 'd, 'i, 'v> InputLength for ParseState<'c, 'd, 'i, 'v> {
    fn input_len(&self) -> usize {
        self.contents.input_len()
    }
}

impl<'c, 'd, 'i, 'v> InputIter for ParseState<'c, 'd, 'i, 'v> {
    type Item = char;
    type Iter = CharIndices<'c>;
    type IterElem = Chars<'c>;

    fn iter_indices(&self) -> Self::Iter {
        self.contents.char_indices()
    }

    fn iter_elements(&self) -> Self::IterElem {
        self.contents.chars()
    }

    fn position<P>(&self, predicate: P) -> Option<usize>
    where
        P: Fn(Self::Item) -> bool,
    {
        for (o, c) in self.iter_indices() {
            if predicate(c) {
                return Some(o);
            }
        }
        None
    }

    fn slice_index(&self, count: usize) -> Result<usize, nom::Needed> {
        let mut i = 0;
        for (index, _) in self.iter_indices() {
            if i == count {
                return Ok(index);
            }
            i += 1
        }

        if i == count {
            return Ok(self.contents.len());
        }

        Err(nom::Needed::Unknown)
    }
}

impl<'c, 'd, 'i, 'v> InputTake for ParseState<'c, 'd, 'i, 'v> {
    fn take(&self, count: usize) -> Self {
        Self {
            contents: &self.contents[..count],
            ..*self
        }
    }

    fn take_split(&self, count: usize) -> (Self, Self) {
        let (prefix, suffix) = self.contents.split_at(count);
        (
            ParseState {
                contents: suffix,
                ..*self
            },
            ParseState {
                contents: prefix,
                ..*self
            },
        )
    }
}

impl<'c, 'd, 'i, 'v, T> Slice<T> for ParseState<'c, 'd, 'i, 'v>
where
    &'c str: Slice<T>,
{
    fn slice(&self, range: T) -> Self {
        Self {
            contents: self.contents.slice(range),
            ..*self
        }
    }
}

impl<'c, 'd, 'i, 'v> UnspecializedInput for ParseState<'c, 'd, 'i, 'v> {}

#[derive(Debug, thiserror::Error)]
pub enum ParseError {
    #[error("the final line in the file ended with a continuation: {0}")]
    ContinuationOnFinalLine(String),
    #[error(transparent)]
    Expansion(#[from] cromulent::WordError),
    #[error("file {0} was included twice")]
    FileIncludedTwice(std::path::PathBuf),
    #[error("variable name is invalid: {0}")]
    InvalidVariable(String),
    #[error(transparent)]
    IoError(#[from] std::io::Error),
    #[error("path `{0}` has no file name")]
    NoFileName(std::path::PathBuf),
    #[error("path `{0}` has no parent directory")]
    NoParentDirectory(std::path::PathBuf),
    #[error(transparent)]
    NulError(#[from] std::ffi::NulError),
    #[error("syntax error: {0:?} {1}")]
    SyntaxError(nom::error::ErrorKind, String),
    #[error("path `{0}` could not be cast into a canonical form: {1}")]
    UnableToCanonicalize(std::path::PathBuf, std::io::Error),
    #[error("could not parse input: {0}")]
    UnrecognizedInput(String),
}

impl<'c, 'd, 'i, 'v> nom::error::ParseError<ParseState<'c, 'd, 'i, 'v>> for ParseError {
    fn from_error_kind(input: ParseState<'c, 'd, 'i, 'v>, kind: nom::error::ErrorKind) -> Self {
        Self::SyntaxError(kind, input.contents.into())
    }

    fn append(
        _input: ParseState<'c, 'd, 'i, 'v>,
        _kind: nom::error::ErrorKind,
        other: Self,
    ) -> Self {
        other
    }
}

type IResult<I, O> = nom::IResult<I, O, ParseError>;

fn fail<E: Into<ParseError>>(error: E) -> nom::Err<ParseError> {
    nom::Err::Failure(error.into())
}

pub trait Parseable {
    // TODO: Return `Result`s.
    // TODO: Is it sensible to expect references?
    fn contents(&self) -> String;
    fn directory(&self) -> Option<std::path::PathBuf>;
    fn path(&self) -> Option<std::path::PathBuf>;
}

impl Parseable for &std::path::Path {
    fn contents(&self) -> String {
        let mut contents = String::new();
        let mut file = std::fs::File::open(self).unwrap();
        file.read_to_string(&mut contents).unwrap();

        contents
    }

    fn directory(&self) -> Option<std::path::PathBuf> {
        Some(
            self.parent()
                .expect("Path must be a file to be read from.")
                .into(),
        )
    }

    fn path(&self) -> Option<std::path::PathBuf> {
        Some(self.into())
    }
}

impl Parseable for &str {
    fn contents(&self) -> String {
        self.to_string()
    }

    fn directory(&self) -> Option<std::path::PathBuf> {
        None
    }

    fn path(&self) -> Option<std::path::PathBuf> {
        None
    }
}

pub fn parse<P: Parseable>(parseable: P) -> Result<Vec<Configuration>, ParseError> {
    let included = Default::default();
    let variables = Default::default();

    inner_parse(parseable, &included, &variables).map_err(|error| match error {
        nom::Err::Error(error) | nom::Err::Failure(error) => error,
        nom::Err::Incomplete(_) => unreachable!("We only use the `complete` parsers."),
    })
}

fn inner_parse<P: Parseable>(
    parseable: P,
    included: &std::cell::RefCell<std::collections::HashSet<std::path::PathBuf>>,
    variables: &HashMap<VariableName, VariableValue>,
) -> Result<Vec<Configuration>, nom::Err<ParseError>> {
    // TODO: Leverage the type-system to not even allow this.
    assert!(!parseable
        .path()
        .map(|path| included.borrow().contains(&path))
        .unwrap_or(false));

    #[allow(clippy::collapsible_else_if)]
    if let Some(directory) = parseable.directory() {
        if let Some(file) = parseable
            .path()
            .as_deref()
            .and_then(std::path::Path::file_name)
        {
            log::debug!(
                "Parsing file `{}` in directory `{}`.",
                file.to_string_lossy(),
                directory.display()
            );
        } else {
            log::debug!("Parsing string in directory `{}`.", directory.display());
        }
    } else {
        if let Some(file) = parseable
            .path()
            .as_deref()
            .and_then(std::path::Path::file_name)
        {
            unreachable!(
                "Parsing file `{}` with no directory.",
                file.to_string_lossy()
            );
        } else {
            log::debug!("Parsing string with no directory.");
        }
    }

    let contents = parseable.contents();
    let contents = preprocess_continuations(&contents);

    let new_variables = preprocess_variable_definitions(&contents).map_err(fail)?;
    let variables = {
        let mut variables = variables.clone();
        variables.extend(new_variables.into_iter());
        variables
    };
    let contents = preprocess_variable_uses(&contents, &variables);

    if let Some(path) = parseable.path() {
        included.borrow_mut().insert(path);
    }

    let directory = parseable.directory();
    let directory = directory.as_deref();

    parse_configuration(ParseState {
        contents: &contents,
        directory,
        variables: &variables,
        included,
    })
}

fn preprocess_continuations(contents: &str) -> String {
    let mut result = String::new();
    for line in contents.lines() {
        result.push_str(line);
        if result.ends_with('\\') && !line.starts_with('#') {
            log::trace!("Line continuation found: {}", line);
            // Remove the backslash and _don't_ add a newline, since the next line should be
            // considered part of this line.
            result.pop();
        } else {
            if result.ends_with('\\') {
                log::info!("Ignoring continuation in comment: {}", line);
            }
            result.push('\n');
        }
    }
    result
}

fn preprocess_variable_definitions(
    contents: &str,
) -> Result<HashMap<VariableName, VariableValue>, ParseError> {
    let mut variables = HashMap::default();
    let set = Regex::new(r"(?i)^set\s+(?P<name>\S+)\s+(?P<value>.+)$").expect("Static regex.");
    let set_from_resource =
        Regex::new(r"(?i)^set_from_resource\s+(?P<name>\S+)\s+(?P<key>\S+)\s*(?P<fallback>\S*)$")
            .expect("Static regex.");

    for line in contents.lines() {
        let (name, value): (String, _) = if let Some(captures) = set.captures(line) {
            log::debug!("Found `set` variable: {}", line);

            let name = captures
                .name("name")
                .expect("`name` is a required group.")
                .as_str()
                .into();
            let value = captures
                .name("value")
                .expect("`value` is a required group.")
                .as_str()
                .into();

            (name, value)
        } else if let Some(captures) = set_from_resource.captures(line) {
            log::debug!("Found `set_from_resource` variable: {}", line);

            let name = captures
                .name("name")
                .expect("`name` is a required group.")
                .as_str()
                .into();
            let key = captures
                .name("key")
                .expect("`key` is a required group.")
                .as_str();
            let fallback = captures
                .name("fallback")
                .expect("`fallback` is a required group.")
                .as_str()
                .into();

            log::warn!(
                "Setting variables from X resources is not supported, fallback will be used: Key \
                 {} Fallback {}",
                key,
                fallback
            );

            (name, fallback)
        } else {
            continue;
        };

        if !name.starts_with('$') {
            return Err(ParseError::InvalidVariable(name));
        }

        variables.insert(VariableName(name), VariableValue(value));
    }

    Ok(variables)
}

fn preprocess_variable_uses(
    contents: &str,
    variables: &HashMap<VariableName, VariableValue>,
) -> String {
    let regices = {
        let mut regices = variables
            .iter()
            .map(|(k, _)| {
                let regex = format!(r"(?i){}", regex::escape(&k.0));
                log::trace!("Variable name regex: {}", regex);

                (
                    k,
                    Regex::new(&regex).expect(
                        "Any characters that could have caused problems should have been escaped.",
                    ),
                )
            })
            .collect::<Vec<_>>();

        // Sort by the reverse length of the key names, so that we ensure variables that
        // are a prefix of other variables do not get matched before the variable that
        // they are a prefix of.
        regices.sort_unstable_by_key(|(key, _)| key.0.len());
        regices.reverse();

        regices
    };

    let mut result = String::new();
    let mut previous = 0;
    while previous < contents.len() {
        let next = regices
            .iter()
            .filter_map(|(k, r)| r.find(&contents[previous..]).map(|m| (k, m)))
            .min_by_key(|(_, v)| v.start());

        if let Some((name, r#match)) = next {
            let start = previous + r#match.start();
            // Copy everything up to the start of the variable.
            result.push_str(&contents[previous..start]);
            // Add the variables value.
            result.push_str(&variables[name].0);
            // Start at the end of the match on the next loop.
            previous += r#match.end();
        } else {
            // No matches left, so just copy the rest.
            result.push_str(&contents[previous..]);
            previous = contents.len();
        }
    }

    result
}

// TODO: Have a separate `ModeConfiguration` type.
#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
pub enum Configuration {
    Bar(Vec<BarConfiguration>),
    Binding {
        key: String,
        command: String,
        group: Option<BindingGroup>,
        modifiers: Vec<BindingModifier>,
        options: Vec<BindingOption>,
    },
    Comment(String),
    Include(Include),
    Mode {
        name: String,
        pango: bool,
        configuration: Vec<Configuration>,
    },
    Other {
        directive: String,
        // TODO: Parse arguments properly.
        arguments: String,
    },
}

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
pub enum BarConfiguration {
    Colors(Vec<ColorDirective>),
    Comment(String),
    Other {
        directive: String,
        arguments: String,
    },
}

// NOTE: Not using the `From` trait because this should be private.
impl BarConfiguration {
    fn from(configuration: Configuration) -> Self {
        use Configuration::*;
        match configuration {
            Comment(comment) => Self::Comment(comment),
            Other {
                directive,
                arguments,
            } => Self::Other {
                directive,
                arguments,
            },
            _ => unreachable!("The parser should have prevented this."),
        }
    }
}

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
pub struct ColorDirective {
    pub class: String,
    pub colors: Vec<String>,
}

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
pub enum Include {
    /// If the configuration file has a directory then the pattern will be
    /// expanded and included files will be parsed.
    Configuration(Vec<Configuration>),
    /// If the configuration file has no directory associated with it, then the
    /// pattern will be left as is.
    Pattern(String),
}

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
pub enum BindingOption {
    Release,
    Border,
    WholeWindow,
    ExcludeTitlebar,
    Other(String),
}

impl Display for BindingOption {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use BindingOption::*;
        match self {
            Release => write!(f, "Release"),
            Border => write!(f, "Border"),
            WholeWindow => write!(f, "Whole Window"),
            ExcludeTitlebar => write!(f, "Exclude Titlebar"),
            Other(option) => write!(f, "{}", option),
        }
    }
}

#[derive(Clone, Copy, Debug, Deserialize, Eq, PartialEq, Serialize)]
pub struct BindingGroup(pub u8);

impl Display for BindingGroup {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Group{}", self.0)
    }
}

#[derive(Clone, Copy, Debug, Deserialize, Eq, PartialEq, Serialize)]
pub enum BindingModifier {
    Mod(u8),
    Shift,
    Control,
}

impl Display for BindingModifier {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use BindingModifier::*;
        match self {
            Mod(value) => write!(f, "Mod{}", value),
            Shift => write!(f, "Shift"),
            Control => write!(f, "Ctrl"),
        }
    }
}

fn parse_configuration(input: ParseState) -> Result<Vec<Configuration>, nom::Err<ParseError>> {
    let (input, _) = multispace0(input)?;

    many0(map(pair(configuration, multispace0), |(c, _)| c))(input).map(|(_, v)| v)
}

fn configuration<'c, 'd, 'i, 'v>(
    input: ParseState<'c, 'd, 'i, 'v>,
) -> IResult<ParseState<'c, 'd, 'i, 'v>, Configuration> {
    // NOTE: `other` _must_ go at the end!
    alt((bar, binding, comment, include, mode, other))(input)
}

fn bar<'c, 'd, 'i, 'v>(
    input: ParseState<'c, 'd, 'i, 'v>,
) -> IResult<ParseState<'c, 'd, 'i, 'v>, Configuration> {
    let (input, _) = space0(input)?;
    let (input, _) = tag("bar")(input)?;
    let (input, _) = space0(input)?;
    let (input, _) = tag("{")(input)?;
    let (input, _) = multispace0(input)?;
    let (input, configuration) = many0(map(
        pair(
            alt((
                colors,
                map(comment, BarConfiguration::from),
                map(other, BarConfiguration::from),
            )),
            multispace0,
        ),
        |(bc, _)| bc,
    ))(input)?;
    let (input, _) = tag("}")(input)?;

    log::trace!("Parsed bar: {:?}", configuration);
    Ok((input, Configuration::Bar(configuration)))
}

fn binding<'c, 'd, 'i, 'v>(
    input: ParseState<'c, 'd, 'i, 'v>,
) -> IResult<ParseState<'c, 'd, 'i, 'v>, Configuration> {
    fn option<'c, 'd, 'i, 'v>(
        input: ParseState<'c, 'd, 'i, 'v>,
    ) -> IResult<ParseState<'c, 'd, 'i, 'v>, BindingOption> {
        let (input, _) = space0(input)?;
        let (input, _) = tag("--")(input)?;
        let (input, option) = is_not(" \t")(input)?;

        let option = option.contents.to_lowercase();
        let option = match option.as_str() {
            "release" => BindingOption::Release,
            "border" => BindingOption::Border,
            "whole-window" => BindingOption::WholeWindow,
            "exclude-titlebar" => BindingOption::ExcludeTitlebar,
            _ => BindingOption::Other(option),
        };

        log::trace!("Parsed binding option: {:?}", option);
        Ok((input, option))
    }

    fn group<'c, 'd, 'i, 'v>(
        input: ParseState<'c, 'd, 'i, 'v>,
    ) -> IResult<ParseState<'c, 'd, 'i, 'v>, BindingGroup> {
        let (input, group) = alt((
            tag_no_case("group1"),
            tag_no_case("group2"),
            tag_no_case("mode_switch"),
            tag_no_case("group3"),
            tag_no_case("group4"),
        ))(input)?;
        let (input, _) = tag("+")(input)?;

        let group = group.contents.to_lowercase();
        let group = BindingGroup(match group.as_str() {
            "group1" => 1,
            "group2" | "mode_switch" => 2,
            "group3" => 3,
            "group4" => 4,
            _ => unreachable!("Parser should have already rejected this."),
        });

        log::trace!("Parsed binding group: {:?}", group);
        Ok((input, group))
    }

    fn modifier<'c, 'd, 'i, 'v>(
        input: ParseState<'c, 'd, 'i, 'v>,
    ) -> IResult<ParseState<'c, 'd, 'i, 'v>, BindingModifier> {
        let (input, modifier) = alt((
            tag_no_case("mod1"),
            tag_no_case("mod2"),
            tag_no_case("mod3"),
            tag_no_case("mod4"),
            tag_no_case("mod5"),
            tag_no_case("shift"),
            tag_no_case("control"),
            tag_no_case("ctrl"),
        ))(input)?;
        let (input, _) = tag("+")(input)?;

        let modifier = modifier.contents.to_lowercase();
        let modifier = match modifier.as_str() {
            "mod1" => BindingModifier::Mod(1),
            "mod2" => BindingModifier::Mod(2),
            "mod3" => BindingModifier::Mod(3),
            "mod4" => BindingModifier::Mod(4),
            "mod5" => BindingModifier::Mod(5),
            "shift" => BindingModifier::Shift,
            "control" | "ctrl" => BindingModifier::Control,
            _ => unreachable!("Parser should have already rejected this."),
        };

        log::trace!("Parsed binding modifier: {:?}", modifier);
        Ok((input, modifier))
    }

    let (input, _) = space0(input)?;
    let (input, _) = alt((tag("bindsym"), tag("bindcode")))(input)?;
    let (input, options) = many0(option)(input)?;
    let (input, _) = space0(input)?;
    let (input, group) = opt(group)(input)?;
    let (input, modifiers) = many0(modifier)(input)?;
    let (input, key) = alphanumeric1(input)?;
    let (input, _) = space0(input)?;
    // TODO: Parse the actual command into a command enum.
    let (input, command) = not_line_ending(input)?;
    let (input, _) = line_ending(input)?;

    let binding = Configuration::Binding {
        key: key.contents.into(),
        command: command.contents.into(),
        group,
        modifiers,
        options,
    };

    log::trace!("Parsed binding: {:?}", binding);
    Ok((input, binding))
}

fn colors<'c, 'd, 'i, 'v>(
    input: ParseState<'c, 'd, 'i, 'v>,
) -> IResult<ParseState<'c, 'd, 'i, 'v>, BarConfiguration> {
    fn color_directive<'c, 'd, 'i, 'v>(
        input: ParseState<'c, 'd, 'i, 'v>,
    ) -> IResult<ParseState<'c, 'd, 'i, 'v>, ColorDirective> {
        fn color<'c, 'd, 'i, 'v>(
            input: ParseState<'c, 'd, 'i, 'v>,
        ) -> IResult<ParseState<'c, 'd, 'i, 'v>, String> {
            let (input, _) = space0(input)?;
            let (input, _) = tag("#")(input)?;
            let (input, color) = alphanumeric1(input)?;
            let (input, _) = space0(input)?;

            log::trace!("Parsed color: {}", color.contents);
            Ok((input, color.contents.into()))
        }

        let (input, _) = space0(input)?;
        let (input, class) = is_not(" \t")(input)?;
        let (input, _) = space0(input)?;
        let (input, colors) = many0(color)(input)?;
        let (input, _) = line_ending(input)?;

        let directive = ColorDirective {
            class: class.contents.into(),
            colors,
        };

        log::trace!("Parsed color directive: {:?}", directive);
        Ok((input, directive))
    }

    let (input, _) = space0(input)?;
    let (input, _) = tag("colors")(input)?;
    let (input, _) = space0(input)?;
    let (input, _) = tag("{")(input)?;
    let (input, _) = multispace0(input)?;
    let (input, directives) = many0(map(pair(color_directive, multispace0), |(cd, _)| cd))(input)?;
    let (input, _) = tag("}")(input)?;

    log::trace!("Parsed colors block: {:?}", directives);
    Ok((input, BarConfiguration::Colors(directives)))
}

fn comment<'c, 'd, 'i, 'v>(
    input: ParseState<'c, 'd, 'i, 'v>,
) -> IResult<ParseState<'c, 'd, 'i, 'v>, Configuration> {
    let (input, _) = space0(input)?;
    let (input, _) = tag("#")(input)?;
    let (input, _) = space0(input)?;
    let (input, text) = not_line_ending(input)?;
    let (input, _) = line_ending(input)?;

    let text = text.contents.into();

    log::trace!("Parsed comment: {}", text);
    Ok((input, Configuration::Comment(text)))
}

fn include<'c, 'd, 'i, 'v>(
    input: ParseState<'c, 'd, 'i, 'v>,
) -> IResult<ParseState<'c, 'd, 'i, 'v>, Configuration> {
    let (input, _) = space0(input)?;
    let (input, _) = tag("include")(input)?;
    let (input, _) = space0(input)?;
    let (input, pattern) = string(input)?;
    let (input, _) = line_ending(input)?;

    log::trace!("Parsed include directive: {}", pattern);

    let include = if let Some(directory) = input.directory {
        let paths = {
            let _lock = CHDIR_MUTEX.lock().unwrap();

            let old_directory = std::env::current_dir().map_err(fail)?;

            log::debug!("Moving to directory: {}", directory.display());
            std::env::set_current_dir(directory).map_err(fail)?;

            let words = cromulent::WordExpander::default()
                .allow_commands()
                .allow_undefined()
                .expand(pattern)
                .map_err(fail)?;

            let paths = words
                .path()
                .into_iter()
                .map(|path| {
                    path.canonicalize()
                        .map_err(|error| ParseError::UnableToCanonicalize(path.into(), error))
                })
                .collect::<Result<Vec<_>, _>>()
                .map_err(fail)?;

            log::debug!("Moving back to directory: {}", old_directory.display());
            std::env::set_current_dir(old_directory).map_err(fail)?;

            paths
        };

        let mut configuration = vec![];

        for path in &paths {
            log::trace!("Expanded word: {}", path.display());

            if input.included.borrow().contains(path) {
                continue;
            }

            configuration.extend(inner_parse(
                path.as_path(),
                input.included,
                input.variables,
            )?);
        }

        Include::Configuration(configuration)
    } else {
        Include::Pattern(pattern)
    };

    Ok((input, Configuration::Include(include)))
}

fn mode<'c, 'd, 'i, 'v>(
    input: ParseState<'c, 'd, 'i, 'v>,
) -> IResult<ParseState<'c, 'd, 'i, 'v>, Configuration> {
    let (input, _) = space0(input)?;
    let (input, _) = tag("mode")(input)?;
    let (input, _) = space0(input)?;
    let (input, pango) = opt(tag("--pango_markup"))(input)?;
    let (input, _) = space0(input)?;
    let (input, name) = word(input)?;
    let (input, _) = space0(input)?;
    let (input, _) = tag("{")(input)?;
    let (input, _) = multispace0(input)?;
    let (input, configuration) =
        many0(map(pair(alt((binding, comment)), multispace0), |(c, _)| c))(input)?;
    let (input, _) = tag("}")(input)?;

    let configuration = Configuration::Mode {
        name,
        pango: pango.is_some(),
        configuration,
    };

    log::trace!("Parsed mode block: {:?}", configuration);
    Ok((input, configuration))
}

fn other<'c, 'd, 'i, 'v>(
    input: ParseState<'c, 'd, 'i, 'v>,
) -> IResult<ParseState<'c, 'd, 'i, 'v>, Configuration> {
    let (input, _) = space0(input)?;
    let (input, directive) = identifier(input)?;
    let (input, _) = space0(input)?;
    let (input, arguments) = not_line_ending(input)?;
    let (input, _) = line_ending(input)?;

    let configuration = Configuration::Other {
        directive,
        arguments: arguments.contents.into(),
    };

    log::trace!("Parsed other: {:?}", configuration);
    Ok((input, configuration))
}

fn identifier<'c, 'd, 'i, 'v>(
    input: ParseState<'c, 'd, 'i, 'v>,
) -> IResult<ParseState<'c, 'd, 'i, 'v>, String> {
    let (input, identifier) = input.split_at_position1_complete(
        |item| !item.is_alphanum() && item != '_' && item != '.',
        nom::error::ErrorKind::AlphaNumeric,
    )?;
    let identifier = identifier.contents.into();

    log::trace!("Parsed identifier: {}", identifier);
    Ok((input, identifier))
}

fn quoted<'c, 'd, 'i, 'v>(
    input: ParseState<'c, 'd, 'i, 'v>,
) -> IResult<ParseState<'c, 'd, 'i, 'v>, String> {
    let (input, _) = tag("\"")(input)?;
    let (input, text) = many1(tuple((is_not("\\\""), opt(tag("\\\\\"")))))(input)?;
    let (input, _) = tag("\"")(input)?;

    Ok((
        input,
        text.iter()
            .flat_map(|(text, quote)| {
                [
                    text.contents,
                    quote.as_ref().map(|t| t.contents).unwrap_or(""),
                ]
            })
            .collect::<Vec<_>>()
            .join(""),
    ))
}

fn string<'c, 'd, 'i, 'v>(
    input: ParseState<'c, 'd, 'i, 'v>,
) -> IResult<ParseState<'c, 'd, 'i, 'v>, String> {
    fn unquoted<'c, 'd, 'i, 'v>(
        input: ParseState<'c, 'd, 'i, 'v>,
    ) -> IResult<ParseState<'c, 'd, 'i, 'v>, ParseState<'c, 'd, 'i, 'v>> {
        is_not("\0\r\n")(input)
    }

    alt((quoted, map(unquoted, |t| t.contents.into())))(input)
}

fn word<'c, 'd, 'i, 'v>(
    input: ParseState<'c, 'd, 'i, 'v>,
) -> IResult<ParseState<'c, 'd, 'i, 'v>, String> {
    fn unquoted<'c, 'd, 'i, 'v>(
        input: ParseState<'c, 'd, 'i, 'v>,
    ) -> IResult<ParseState<'c, 'd, 'i, 'v>, ParseState<'c, 'd, 'i, 'v>> {
        is_not(" \t],;\r\n\0")(input)
    }

    alt((quoted, map(unquoted, |t| t.contents.into())))(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn preprocess_one_variable() {
        let initial = "workspace $foo";
        let variables = [(VariableName("$foo".into()), VariableValue("bar".into()))]
            .into_iter()
            .collect::<HashMap<_, _>>();
        assert_eq!(
            preprocess_variable_uses(initial, &variables),
            "workspace bar"
        );
    }

    #[test]
    fn preprocess_one_variable_at_start() {
        let initial = "$foo";
        let variables = [(
            VariableName("$foo".into()),
            VariableValue("workspace bar".into()),
        )]
        .into_iter()
        .collect::<HashMap<_, _>>();
        assert_eq!(
            preprocess_variable_uses(initial, &variables),
            "workspace bar"
        );
    }

    #[test]
    fn preprocess_variable_with_dollar_sign() {
        let initial = "$foo$bar";
        let variables = [(
            VariableName("$foo$bar".into()),
            VariableValue("workspace bar".into()),
        )]
        .into_iter()
        .collect::<HashMap<_, _>>();
        assert_eq!(
            preprocess_variable_uses(initial, &variables),
            "workspace bar"
        );
    }

    #[test]
    fn preprocess_two_variables() {
        let initial = "$foo $bar";
        let variables = [
            (
                VariableName("$foo".into()),
                VariableValue("workspace".into()),
            ),
            (VariableName("$bar".into()), VariableValue("bar".into())),
        ]
        .into_iter()
        .collect::<HashMap<_, _>>();
        assert_eq!(
            preprocess_variable_uses(initial, &variables),
            "workspace bar"
        );
    }

    #[test]
    fn preprocess_two_variables_back_to_back() {
        let initial = "$foo$bar";
        let variables = [
            (VariableName("$foo".into()), VariableValue("work".into())),
            (
                VariableName("$bar".into()),
                VariableValue("space bar".into()),
            ),
        ]
        .into_iter()
        .collect::<HashMap<_, _>>();
        assert_eq!(
            preprocess_variable_uses(initial, &variables),
            "workspace bar"
        );
    }

    #[test]
    fn preprocess_non_variable_with_dollar_sign() {
        let initial = "workspace $foo";
        let variables = Default::default();
        assert_eq!(
            preprocess_variable_uses(initial, &variables),
            "workspace $foo"
        );
    }
}
